﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IndyFurConCore.Web.Models {
	public class Staff {
        public int Id { get; set; }
        public string RealName { get; set; }

        private string mName;
        public string Name {
            get { return mName; }
            set {
                if (string.IsNullOrWhiteSpace(RealName)) {
                    mName = value;
                } else {
                    mName = $"{value} ({RealName})";
                }
            }
        }
		
		[EmailAddress]
		public string EmailAddress { get; set; }

        public string ImageUrl { get; set; }

        public string Title { get; set; }

        public string Bio { get; set; }
    }

    public class StaffCollection {
        public List<Staff> Board { get; set; }
        public List<Staff> Staff { get; set; }
    }
}