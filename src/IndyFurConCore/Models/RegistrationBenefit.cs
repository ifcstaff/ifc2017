﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndyFurConCore.Models {
    public class RegistrationBenefit {
        public bool UseIcon { get; set; } = true;
        public string Icon { get; set; }
        public string Text { get; set; }

        public RegistrationBenefit(string icon, string text, bool useIcon = true) {
            Icon = icon;
            Text = text;
            UseIcon = useIcon;
        }
    }
}
