﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IndyFurConCore.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace IndyFurConCore.Models
{
	public class JsonStaffQuery : IStaffQuery {
		private readonly IHostingEnvironment mEnv;
		private readonly string mFilePath;
		private readonly IFileInfo mFileInfo;

		public JsonStaffQuery(IHostingEnvironment env) {
			mEnv = env;
			mFilePath = Path.Combine(mEnv.WebRootPath, "StaffExample.json");
			var provider = new PhysicalFileProvider(mEnv.WebRootPath);
			mFileInfo = provider.GetFileInfo("StaffExample.json");
		}

		public StaffCollection GetStaff() {
			string file;
			using (StreamReader stream = new StreamReader(mFileInfo.CreateReadStream())) {
				file = stream.ReadToEnd();
			}

			StaffCollection staff = JsonConvert.DeserializeObject<StaffCollection>(file);

			return staff;
		}
	}
}
