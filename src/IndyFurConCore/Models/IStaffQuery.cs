﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IndyFurConCore.Web.Models;
using Microsoft.AspNetCore.Hosting;

namespace IndyFurConCore.Models {
	public interface IStaffQuery {
		StaffCollection GetStaff();
	}
}
