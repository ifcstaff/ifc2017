﻿//Cheers, love!
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace IndyFurConCore.Util {
	public class Tracer : ITracer {
		private readonly IHostingEnvironment mEnv;
		//private string mFilePath;

		public Tracer(IHostingEnvironment env) {
			mEnv = env;
			//mFilePath = Path.Combine(mEnv.WebRootPath, "Log.txt");
		}

		public void Initialize() {
			//FileStream stream = new FileStream(mFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
			//if (!File.Exists(mFilePath) && stream.CanRead && stream.CanWrite) {
			//	try {
			//		var innerStream = File.Create(mFilePath);

			//		using (var logWriter = new StreamWriter(innerStream)) {
			//			if (innerStream.CanWrite) {
			//				logWriter.WriteLine($"Log Created at ({UtilMethods.GetNowTimeStamp()})");
			//			}
			//		}

			//		innerStream.Dispose();
			//	} catch {
			//		Console.WriteLine("Could not create log file. :c");
			//	}
			//}
			//stream.Dispose();

			//stream = new FileStream(mFilePath, FileMode.Append, FileAccess.Write);
			//if (stream.CanWrite) {
			//	var logWriter = new StreamWriter(stream);

			//	logWriter.WriteLine($"\nApplication Started! ({UtilMethods.GetNowTimeStamp()})");
			//	logWriter.Dispose();
			//}

			//stream.Dispose();
		}


		public void Error(string message) {
			WriteToLog(new Log("Error", ConsoleColor.DarkMagenta, message));
		}

		public void Exception(Exception e, string message) {
			WriteToLog(new Log("Exception", ConsoleColor.Red, message + ", " + e.ToString()));
		}

		public void ExceptionWarning(Exception e, string message) {
			WriteToLog(new Log("Exception", ConsoleColor.Yellow, message + ", " + e.ToString()));
		}

		public void Info(string message) {
			WriteToLog(new Log("Info", ConsoleColor.Cyan, message));
		}

		public void Todo(string message) {
			WriteToLog(new Log("Todo", ConsoleColor.Green, message));
		}

		public void Verbose(string message) {
			WriteToLog(new Log("Verbose", ConsoleColor.White, message));
		}

		public void Warning(string message) {
			WriteToLog(new Log("Warning", ConsoleColor.Yellow, message));
		}

		private void WriteToLog(Log log) {
			//using (var stream = new FileStream(mFilePath, FileMode.Append, FileAccess.Write)) {
			//	if (File.Exists(mFilePath) && stream.CanWrite) {
			//		using (StreamWriter writer = new StreamWriter(stream)) {
			//			writer.WriteLine(log);
			//		}
			//	}
			//}
			Console.ForegroundColor = log.ConsoleColor;
			Console.WriteLine(log);

			Console.ForegroundColor = ConsoleColor.Gray;
		}

		public class Log {
			public string LogType { get; set; }
			public ConsoleColor ConsoleColor { get; set; }
			public string Message { get; set; }


			public Log(string logType, ConsoleColor color, string message) {
				LogType = logType;
				Message = message;
				ConsoleColor = color;
			}

			public override string ToString() {
				return $"[{UtilMethods.GetNowTimeStamp()}({LogType})]: {Message}";
			}
		}
	}
}