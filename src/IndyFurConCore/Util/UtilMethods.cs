﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace IndyFurConCore.Util {
	public class UtilMethods {
		public static string GetNowTimeStamp() {
			return DateTime.Now.ToString(Constants.Date.Format, CultureInfo.InvariantCulture);
		}

	}
}
