﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IndyFurConCore.Util {
	public static class MiscExtensions {
		public static string ToHtmlString(this TagBuilder builder) {
			using (var writer = new System.IO.StringWriter()) {
				builder.WriteTo(writer, HtmlEncoder.Default);
				return writer.ToString();
			}
			
		}
	}
}
