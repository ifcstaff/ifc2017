﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace IndyFurConCore.Util {
	public class AppSettingsAccess : IAppSettingsAccess {
		private readonly IConfigurationRoot mConfigurationRoot;
		private readonly ITracer mTracer;

		public AppSettingsAccess(IConfigurationRoot configurationRoot, ITracer tracer) {
			mConfigurationRoot = configurationRoot;
			mTracer = tracer;
		}

		public string GetSetting(string key, string defaultValue = null) {
			string value = defaultValue;
			
			try {
				value = mConfigurationRoot[key.Replace('.', ':')];
			} catch (Exception exc) {
				mTracer.Exception(exc, $"An exception occured while trying to get app setting (\"{key}\")");
			}

			return value;
		}

		public T GetSetting<T>(string key, T defaultVal) {
			T value = defaultVal;

			try {
				value = mConfigurationRoot.GetValue<T>(key.Replace('.', ':'));
			} catch (Exception exc) {
				mTracer.Exception(exc, $"An exception occured while trying to get app setting (\"{key}\") or while converting it to a (\"{typeof(T)}\")");
			}

			return value;
		}
	}
}
