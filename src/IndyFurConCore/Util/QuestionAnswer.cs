﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace IndyFurConCore.Util {
    public class QuestionAnswer {
        public string Question { get; set; }
        public string Answer { get; set; }

        public QuestionAnswer(string question, string answer) {
            Question = question;
            Answer = answer;
        }
    }
}
