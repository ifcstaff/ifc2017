﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace IndyFurConCore.Util {
    public class MailToTagHelper : TagHelper {

		public string Email { get; set; }

        public string Display { get; set; } = null;

		public override void Process(TagHelperContext context, TagHelperOutput output) {
			TagBuilder tagBuilder = new TagBuilder("a");
			tagBuilder.InnerHtml.Append(Display ?? Email);
			tagBuilder.MergeAttribute("href", $"mailto:{Email}");
			tagBuilder.TagRenderMode = TagRenderMode.Normal;
            Console.WriteLine(output.TagMode);

            //output.TagMode = output.Content.IsEmptyOrWhiteSpace ? TagMode.SelfClosing : TagMode.StartTagAndEndTag;
            //this.
            //output.TagMode = TagMode.StartTagAndEndTag | TagMode.SelfClosing;
			output.Content.SetHtmlContent(tagBuilder.ToHtmlString());
		}
    }
}