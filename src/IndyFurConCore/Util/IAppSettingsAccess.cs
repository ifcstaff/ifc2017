﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndyFurConCore.Util {
	public interface IAppSettingsAccess {
		string GetSetting(string key, string defaultVal = null);
		T GetSetting<T>(string key, T defaultVal);
	}
}
