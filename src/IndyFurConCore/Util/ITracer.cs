﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndyFurConCore.Util {
	public interface ITracer {
		void Initialize();
		void Error(string message);
		void Exception(Exception e, string message);
		void ExceptionWarning(Exception e, string message);
		void Info(string message);
		void Todo(string message);
		void Verbose(string message);
		void Warning(string message);
	}
}
