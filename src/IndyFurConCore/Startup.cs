﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using IndyFurConCore.Models;
using IndyFurConCore.Util;

namespace IndyFurConCore {
	public class Startup {
		public Startup(IHostingEnvironment env) {
			var builder = new ConfigurationBuilder()
				.SetBasePath(env.ContentRootPath)
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
				.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
            
			Configuration = builder.Build();
		}

		public IConfigurationRoot Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services) {
			// Add framework services.

			services.AddMvc();

			// Add application services.
			services.AddTransient<ITracer, Tracer>();
			services.AddTransient<IAppSettingsAccess, AppSettingsAccess>();

			services.AddTransient<IConfigurationRoot, ConfigurationRoot>(svc => {
				var builder = new ConfigurationBuilder()
					.SetBasePath(svc.GetService<IHostingEnvironment>().ContentRootPath)
					.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
					.AddJsonFile($"appsettings.{svc.GetService<IHostingEnvironment>().EnvironmentName}.json", optional: true);
				return (ConfigurationRoot)builder.Build();
			});

#if DEBUG
			services.AddTransient<IStaffQuery, JsonStaffQuery>();
#else
			//TODO from DB
			services.AddTransient<IStaffQuery, JsonStaffQuery>();
#endif



		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
			loggerFactory.AddConsole(Configuration.GetSection("Logging"));
			loggerFactory.AddDebug();

			if (env.IsDevelopment()) {
				app.UseBrowserLink();
			} else {
			}

			app.UseStaticFiles();

			// Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715

			app.UseMvc(routes => {
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");
			});

			//var tracer = serviceProvider.GetService<ITracer>();
			//tracer.Initialize();

			app.UseStaticFiles();

		}
	}
}
