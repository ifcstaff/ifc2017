using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IndyFurConCore.Util;
using Microsoft.AspNetCore.Mvc;

namespace IndyFurConCore.Controllers {
	public class AttendController : Controller {
		private readonly IAppSettingsAccess mAppSettingsAccess;

		public AttendController(IAppSettingsAccess appSettingsAccess) {
			mAppSettingsAccess = appSettingsAccess;
		}

		public IActionResult Hotel() {
			ViewData["ApiKey"] = mAppSettingsAccess.GetSetting(Constants.AppSettings.ApiAccess.Google.ApiKey);
			return View();
		}

		public IActionResult Travel() {
			return View();
		}
        
        public IActionResult Programming() {
            return View();
        }

        public IActionResult DanceCompetition() {
            return View();
        }

		public IActionResult FloorWars() {
			return View();
		}
	}
}