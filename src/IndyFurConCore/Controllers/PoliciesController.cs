using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace IndyFurConCore.Controllers {
	public class PoliciesController : Controller {
		public IActionResult Index() {
			return View();
		}

		public IActionResult ConventionAndConduct() {
			return View();
		}

		public IActionResult MediaAndPress() {
			return View();
		}

		public IActionResult Privacy() {
			return View();
		}
	}
}