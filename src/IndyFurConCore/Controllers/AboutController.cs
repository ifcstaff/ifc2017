﻿using System;
using System.Linq;
using IndyFurConCore.Models;
using IndyFurConCore.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace IndyFurCon.Web.Controllers {
    public class AboutController : Controller {
		private readonly IStaffQuery mStaffQuery;
		public AboutController(IStaffQuery staffQuery) {
			mStaffQuery = staffQuery;
		}
		
        public ActionResult Index() {
            return View();
        }

        public ActionResult Charity() {
            return View();
        }

		// No Models for staff here so we can edit on the server
		public ActionResult Staff() {
            StaffCollection staff = mStaffQuery.GetStaff();

            for (int i = 0; i < staff.Board.Count; i++) {
                staff.Board[i].Id = i;
            }

            for (int i = 0; i < staff.Staff.Count; i++) {
                staff.Staff[i].Id = i;
            }

            staff.Staff = staff.Staff.OrderBy(s => s.Name).ToList();

            return View(staff);
        }

	    public ActionResult Volunteer() {
		    return View();
	    }
    }
}