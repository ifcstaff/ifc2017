﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndyFurConCore {
	public static class Constants {
		public static class Date {
			public const string Format = "MM/dd/yyyy HH:mm:ss zzz";
		}

		public static class LogLevels {
			public const string Verbose = "Verbose";
			public const string Info = "Info";
			public const string Todo = "Todo";
			public const string Warning = "Warning";
			public const string Error = "Error";
			public const string Exception = "Exception";

		}

		public static class AppSettings {
			public static class ApiAccess {
				public static class Google {
					public const string ApiKey = "ApiAccess.Google.ApiKey";
				}
			}
		}
	}
}
